<?php
declare(strict_types=1);

namespace Mistyfiky\NRV2E;

class BitsReader
{
    private $data;

    private $dataPosition;
    
    private $currentByte;
    
    private $bitPosition;
    
    private $dataLength;

    public function __construct(array $data, int $startIdx = 0)
    {
        $this->data = $data;
        $this->dataPosition = $startIdx;
        $this->bitPosition = 0;
        $this->dataLength = count($this->data);
    }

    public function isDataAvailable() : bool
    {
        return $this->dataPosition < $this->dataLength;
    }

    public function readBit() : int
    {
        if (!$this->isDataAvailable()) {
            throw new \OutOfRangeException('No more data available!');
        }

        if (0 === $this->bitPosition) {
            $this->currentByte = $this->data[$this->dataPosition++] & 0xff;
            $this->bitPosition = 8;
        }
        
        return ($this->currentByte >> --$this->bitPosition) & 1;
    }

    public function readByte() : int
    {
        if (!$this->isDataAvailable()) {
            throw new \OutOfRangeException('No more data available!');
        }

        return $this->data[$this->dataPosition++] & 0xff;
    }
}