<?php
declare(strict_types=1);

namespace Mistyfiky\NRV2E;

class NRV2EDecompressor
{
    public static function decompress(string $input) : string
    {
        $data = static::prepareInput($input);
        $output = static::process($data);
        $result = static::prepareOutput($output);
        
        return $result;
    }
    
    private static function prepareInput(string $input) : array
    {
        return array_values(unpack('C*', $input));
    }
    
    private static function prepareOutput(array $output) : string 
    {
        return mb_convert_encoding(pack('C*', ...$output), 'UTF-8' , 'UTF-16LE');
    }
    
    private static function process(array $data) : array
    {
        // first 4 bytes are output size
        $bitsReader = new BitsReader($data, 4);
        $dst = [];
        $olen = 0;
        $last_m_off = 1;

        for (; ;) {
            $m_off = 1;

            if (!$bitsReader->isDataAvailable()) {
                return $dst;
            }

            while (1 === $bitsReader->readBit()) {
                $dst[$olen++] = $bitsReader->readByte();
            }
            for (; ;) {
                if (!$bitsReader->isDataAvailable()) {
                    return $dst;
                }

                $m_off = $m_off * 2 + $bitsReader->readBit();

                if (1 === $bitsReader->readBit()) {
                    break;
                }
                $m_off = ($m_off - 1) * 2 + $bitsReader->readBit();
            }
            if (2 === $m_off) {
                $m_off = $last_m_off;
                $m_len = $bitsReader->readBit();
            } else {
                $m_off = ($m_off - 3) * 256 + $bitsReader->readByte();
                if ($m_off == 1) {
                    break;
                }
                $m_len = ($m_off ^ 1) & 1;
                $m_off >>= 1;
                $last_m_off = ++$m_off;
            }

            if (!$bitsReader->isDataAvailable()) {
                return $dst;
            }

            if ($m_len > 0) {
                $m_len = 1 + $bitsReader->readBit();
            } elseif (1 === $bitsReader->readBit()) {
                $m_len = 3 + $bitsReader->readBit();
            } else {
                $m_len++;
                do {
                    if (!$bitsReader->isDataAvailable()) {
                        return $dst;
                    }
                    $m_len = $m_len * 2 + $bitsReader->readBit();
                    if (!$bitsReader->isDataAvailable()) {
                        return $dst;
                    }
                } while (0 === $bitsReader->readBit());
                $m_len += 3;
            }
            $m_len += $m_off > 0x500 ? 1 : 0;

            $m_pos = $olen - $m_off;
            $dst[$olen++] = $dst[$m_pos++];
            do {
                $dst[$olen++] = $dst[$m_pos++];
            } while (--$m_len > 0);
        }

        return $dst;
    }
}