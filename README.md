# NRV2E PHP decompression library

## Usage
```php
$input = 'compressed...';
$result = NRV2EDecompressor::decompress($input);
```

## Tests
To run tests create 2 files:
* tmp/input.txt - with base64decoded input
* tmp/output.txt - which should be result of decompression of previous file

Then you will be able to test  with `composer test`
