<?php
declare(strict_types=1);

namespace Mistyfiky\NRV2E;

use PHPUnit\Framework\TestCase;

final class NRV2EDecompressorTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testDecompressReturnsFromValidInput(string $input, string $output) : void
    {
        $result = NRV2EDecompressor::decompress($input);

        $this->assertSame($output, $result);
    }

    public function dataProvider() : array
    {
        return [
            [
                base64_decode(file_get_contents(__DIR__ . '/../tmp/input.txt')),
                file_get_contents(__DIR__ . '/../tmp/output.txt'),
            ],
        ];
    }
}
